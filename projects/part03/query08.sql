.read data.sql

WITH FriendCountLeftColumn AS (
	SELECT
		ID1 AS ID,
		count(*) AS FriendCount
	FROM Friend
	GROUP BY ID1
),
FriendCountRightColumn AS (
	SELECT
		ID2 AS ID,
		count(*) AS FriendCount
	FROM Friend
	GROUP BY ID2
),
StudentsWithFriendCount AS (
	SELECT
		Highschooler.ID AS ID,
		FriendCountLeftColumn.FriendCount AS FriendCount1,
		FriendCountRightColumn.FriendCount AS FriendCount2
	FROM Highschooler
	LEFT JOIN FriendCountRightColumn
		ON Highschooler.ID = FriendCountRightColumn.ID
	LEFT JOIN FriendCountLeftColumn
		ON Highschooler.ID = FriendCountLeftColumn.ID
)
SELECT
	ID,
	COALESCE(FriendCount1, 0) + COALESCE(FriendCount2, 0) AS FriendCount
FROM StudentsWithFriendCount
WHERE COALESCE(FriendCount1, 0) + COALESCE(FriendCount2, 0) IN (
	SELECT MAX(COALESCE(FriendCount1, 0) + COALESCE(FriendCount2, 0))
	FROM StudentsWithFriendCount
);