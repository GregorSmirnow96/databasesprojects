.read data.sql

WITH LikesWhichAreFriends AS (
	SELECT
		ID1,
		ID2
	FROM Likes
	WHERE
		ID1 IN (
			SELECT ID1
			FROM Friend
			WHERE
				(Friend.ID1 = Likes.ID1 AND
				Friend.ID2 = Likes.ID2) OR
				(Friend.ID1 = Likes.ID2 AND
				Friend.ID2 = Likes.ID1)
		) OR
		ID1 IN (
			SELECT ID2
			FROM Friend
			WHERE
				(Friend.ID1 = Likes.ID1 AND
				Friend.ID2 = Likes.ID2) OR
				(Friend.ID1 = Likes.ID2 AND
				Friend.ID2 = Likes.ID1)
		)
),
NonmutualLikesWhichAreFriends AS (
	SELECT
		ID1 AS ID1,
		ID2 AS ID2
	FROM LikesWhichAreFriends AS OuterLikes
	WHERE OuterLikes.ID1 NOT IN (
		SELECT InnerLikes.ID2
		FROM LikesWhichAreFriends AS InnerLikes
		WHERE OuterLikes.ID2 = InnerLikes.ID1
	)
)

DELETE FROM Likes
WHERE
	ID1 IN (
		SELECT ID1
		FROM NonmutualLikesWhichAreFriends
	);