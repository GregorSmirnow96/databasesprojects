.read data.sql

WITH TransientLikes AS (
	SELECT
		Likes1.ID1 AS ID1,
		Likes1.ID2 AS ID2,
		Likes2.ID2 AS ID3
	FROM Likes AS Likes1
	LEFT JOIN Likes AS Likes2
		ON Likes1.ID2 = Likes2.ID1
	WHERE
		Likes2.ID1 IS NOT NULL AND
		Likes1.ID1 != Likes2.ID2
)
SELECT
	Person1.name AS Name1,
	Person1.grade AS Grade1,
	Person2.name AS Name2,
	Person2.grade AS Grade2,
	Person3.name AS Name3,
	Person3.grade AS Grade3
FROM TransientLikes
LEFT JOIN Highschooler AS Person1
	ON TransientLikes.ID1 = Person1.ID
LEFT JOIN Highschooler AS Person2
	ON TransientLikes.ID2 = Person2.ID
LEFT JOIN Highschooler AS Person3
	ON TransientLikes.ID3 = Person3.ID;