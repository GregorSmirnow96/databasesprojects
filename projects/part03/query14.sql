.read data.sql

WITH FriendsOfDifferentAges AS (
	SELECT
		Friend1.ID AS ID1,
		Friend1.name,
		Friend1.grade,
		Friend2.ID AS ID2,
		Friend2.name,
		Friend2.grade
	FROM Friend
	LEFT JOIN Highschooler AS Friend1
		ON Friend.ID1 = Friend1.ID
	LEFT JOIN Highschooler AS Friend2
		ON Friend.ID2 = Friend2.ID
	WHERE Friend1.grade - Friend2.grade != 0
),
IDsWithFriendsOfDifferentAges AS (
	SELECT ID1 AS ID
	FROM FriendsOfDifferentAges
	UNION
	SELECT ID2 AS ID
	FROM FriendsOfDifferentAges
),
PeopleWithFriendsOfSameAge AS (
	SELECT *
	FROM Highschooler
	WHERE ID NOT IN (
		SELECT ID
		FROM IDsWithFriendsOfDifferentAges
	)
)
SELECT *
FROM PeopleWithFriendsOfSameAge
ORDER BY Grade, Name